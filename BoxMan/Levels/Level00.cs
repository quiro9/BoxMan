﻿using System;

namespace BoxMan
{
	/// <summary>
	///  menu del juego
	/// </summary>
	public class Level00
	{
		private Level level;

		public Level00 (Level _level)
		{
			level = _level;

			// tiempo de nivel
			level.levelTime = 65;

			// limite de nivel
			level.levelMinX = -100; 
			level.levelMaxX = 920;
			level.levelMinY = -130;
			level.levelMaxY = 700;

			// {tipo, positionX, PositionY, eje, min, max,  velocidad, Widhth, Height, band };
			// tipo: dibuja rectangulo del esenario o objetos;
			// tipo de 0 a 4 rectangulos: 0 negro, 1 azul, 2verde, 3rojo 4 amarillo.
			// de 5 a 9 pinceles: 5 negro, 6 azul, 7 verde, 8 rojo 9 amarillo.
			// de 10 a 29 pinchos: 10 negro, 11 azul, 12 verde, 13rojo, 14 amarillo ...
			// de 30 a 34 monedas ... ( todo posee el mismo orden de colores C/5 valores)
			// de 35 a 39 llaves, 40 a 44 bloques llaves view, 45 a 49 bloques llaves hide
			// de 50 a 54 bandera "gano" ... 55 es un bloque del color del fondo puede ocultar objetos,
			// para cosas raras como aparecer objetos o enemigos usar 56 que es transparente
			// eje: indica movimiento 1 ejeX, 2 EjeY
			// min, max: indica valor minimo y maximo en el eje para que s emueva
			// velocidad: cantidad de pixeles
			//    para objetos estaticos colocar valores minX, maxX, minY,maxY, velocidad en 0
			// band: varia segun el tipo, en plataformas mobiles indica "arriba abajo" o "derha izquierda"
			level.rectangleCant = 27;
			level.rectangleType = new int[ 27, 10] {

				// bloques negra
				{0,-50,100, 0, 0, 0, 0,800,50, 0},
				{0,-50,-60, 0, 0, 0, 0,750,75, 0},
				{0,-200,650, 0, 0, 0, 0,750,200, 0}, 
				{0,360,550, 0, 0, 0, 0,700,300,0},
				{30,450,500, 0, 0, 0, 0,20,30,0}, //monedas Negras
				{30,400,500, 0, 0, 0, 0,20,30,0},

				// bloques azul
				{1,-200,250, 0, 0, 0, 0,900,150,0}, 
				{1,500,350, 0, 0, 0, 0,300,200,0},
				{1,770,420, 0, 0, 0, 0,100,50,0}, 
				{1,-300,350, 1, 220, 600, 2,50,50, 0}, // plataforma azul
				{6,880,400, 0, 0, 0, 0,50,50,0}, //pincel azul
				{31,540,200, 0, 0, 0, 0,20,30,0}, //monedas azules
				{31,500,200, 0, 0, 0, 0,20,30,0},
				{31,460,200, 0, 0, 0, 0,20,30,0},
				{31,830,480, 0, 0, 0, 0,20,30,0},
				{31,-280,400, 0, 0, 0, 0,20,30,0},
				{31,-280,350, 0, 0, 0, 0,20,30,0},
				{31,300,550, 0, 0, 0, 0,20,30,0},
				{31,250,550, 0, 0, 0, 0,20,30,0},


				// varios pinceles "antes de bloques verde"
				{8,150,20, 0, 0, 0, 0,50,50,0},
				{6,230,20, 0, 0, 0, 0,50,50,0}, 
				{5,310,20, 0, 0, 0, 0,50,50,0}, 
				{9,390,20, 0, 0, 0, 0,50,50,0}, 
				{7,470,20, 0, 0, 0, 0,50,50 ,0},

				// bloques verde
				{2,780,-20, 0, 0, 0, 0,100,50,0 },
				{32,-50,-95, 0, 0, 0, 0,20,30, 0}, //moneda verde

				// llegada negra
				{50,100,-130, 0, 0, 0, 0, 40,70, 0}

			};

		}

	}
}

