﻿/*
 	Boxman is develop for Juan Quiroga (quiro9) <https://github.com/quiro90>
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;


namespace BoxMan
{
	/// <summary>
	/// Clase principal del juego: aca se llaman las diferentes clases y se interectua entre ellas
	/// para ejecutar y evaluar y mostrar un resultado.
	/// </summary>
	public class BoxMan : Game
	{
		// --- class ---
		private Player player;
		private PlayerKeys playerkeys;
		private Enemy enemy;
		private Level level;
		private TextEvent textevent;
		private Camera camera;

		// -- grapics vars -- 
		GraphicsDeviceManager graphics;
		public int screenWidth=960, screenHeight=740;
		public SpriteBatch spriteBatch;
		public SpriteFont fontSprite;
		public Color gameColor = new Color(220,221,220);



		// --  global variables ---
		// predefine pocición de la camara de acuerdo a la pantalla
		private float cameraPlayerY, cameraPlayerX;
		// gameCont: acumula tiempo de juego milliseconds
		public int gameCont, gameReload;
		public float deltaTime;

		public BoxMan ()
		{
			graphics = new GraphicsDeviceManager (this);
			Content.RootDirectory = "Content";
			gameCont = 0;
			deltaTime = 0;
			gameReload = 1;
		}

		/// <summary>
		/// Inicializaciones para comenzar el juego
		/// </summary>
		protected override void Initialize ()
		{
			// initialice class
			player = new Player();
			playerkeys = new PlayerKeys();
			enemy = new Enemy ();
			level = new Level ();
			textevent = new TextEvent ();
			camera = new Camera (GraphicsDevice.Viewport);

			base.Initialize ();
		}

		protected override void LoadContent()
		{
			// Redefine los graficos
			GraphicSettings ();
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch (GraphicsDevice);

			// carga el contenido Inicial
			player.Load (this);
			level.Load (this);
			enemy.Load (this);

			fontSprite = Content.Load<SpriteFont> ("MainFont");
			// se define levelWin 9 como inicio
			level.levelWin = 9;

		}

		/// <summary>
		/// Logica del juego
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update (GameTime gameTime)
		{
			// si se recarga el juego
			if (gameReload >= 1) {
				level.ReLoad (enemy, screenWidth, screenHeight);
				enemy.ReLoad ();
				// pocicionamos a player en en "centro" de los valores iniciados de level
				player.ReLoad (level.levelStartX - player.playerWidth / 2, level.levelStartY - player.playerHeight / 3);
				gameReload = 0;
			} 
			// cuando el juego esta en progreso
			else if (level.levelWin == 0) {
				// actualiza enemigo y nivel
				// enemy.Update retorna -1 si pierde player
				level.levelWin = enemy.Update (player, gameCont, level.levelWin);
				level.Update (player, gameCont);
			}

			// toma el estado de teclas del jugador y personaje
			playerkeys.Get (this, player, level);
			player.Update (this);
			// si el jugador esta "dentro del nivel" lo enfoca
			if (player.playerRectangle.Location.Y < level.levelMaxY && player.playerRectangle.Location.Y > level.levelMinY) {
				camera.TargetY ((player.playerRectangle.Location.Y * camera.zoom) + cameraPlayerY);
			}
			if (player.playerRectangle.Location.X < level.levelMaxX && player.playerRectangle.Location.X > level.levelMinX) {
				camera.TargetX ((player.playerRectangle.Location.X * camera.zoom) + cameraPlayerX);
			}
			camera.Update (this);
			this.gameCont = gameTime.TotalGameTime.Milliseconds;
			this.deltaTime = (float)gameTime.TotalGameTime.TotalSeconds;
			base.Update (gameTime);
		}

		/// <summary>
		/// Dibuja en el juego
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw (GameTime gameTime)
		{
			graphics.GraphicsDevice.Clear (gameColor);


			// -- drawing code  --
			// en este caso toda la asociación de pantalla vamos a actualizar según la posición de la camara
			spriteBatch.Begin (SpriteSortMode.Deferred,	BlendState.AlphaBlend, null,null,null,null, camera.transform);

			level.Draw (this.spriteBatch, this.gameColor);
			player.Draw (this.spriteBatch, this.gameColor);
			enemy.Draw (this.spriteBatch, this.gameColor);
			textevent.Control (this, player,level, camera);

			spriteBatch.End();

			base.Draw (gameTime);
		}
			
		/// <summary>
		/// Determina los graficos y modifica todo lo necesario para adaptar el juego a la pantalla
		/// </summary>
		public void GraphicSettings ()
		{
			// orientacion "parado"
			graphics.SupportedOrientations = DisplayOrientation.Portrait;

			//screenWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
			//screenHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
			// load resolution

			graphics.PreferredBackBufferWidth = screenWidth;
			graphics.PreferredBackBufferHeight = screenHeight;
			// autodetect resolution, if it fail get the default conf;

			// read graphics settings
			graphics.IsFullScreen = false;
			graphics.SynchronizeWithVerticalRetrace = false;
			graphics.PreferMultiSampling = true;
			// aply new graphics
			graphics.ApplyChanges ();


			// predesignamos unos valores que tomara siempre la camara respecto a "player"
			cameraPlayerX = ((player.playerWidth/2)*camera.zoom)-(screenWidth/2);
			cameraPlayerY = (player.playerHeight*camera.zoom)+((screenHeight/3)-screenHeight);

		}

	}
}

