﻿/*
 	Boxman is develop for Juan Quiroga (quiro9) <https://github.com/quiro9>
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BoxMan
{
	/// <summary>
	/// Clase Jugador: maneja llamadas iniciales y aspectos basicos del jugador a mostrar.
	/// </summary>
	public class Player
	{

		// --  Vars --
		// Rectangulo y textura de player 
		public Rectangle playerRectangle;
		public Texture2D playerSprite;
		// playerPositionX & playerPositionY: Pocicion X, Y, interactua con "playerMove"
		// playerWidth & playerHeight: tamaño para el juegador
		// playerAnimNumber: numero de sprite
		public int playerWidth, playerHeight, playerAnimNumber;

		// propiedad X e Y para el movimiento
		public int playerMoveX, playerMoveY;

		// playerAction: propiedad de animacion
		public string playerAction;

		// indica el color actual del jugador
		// 0 negro, 1 azul, 2 verde, 3 rojo, 4 amarillo
		public int playerType;


		// una bandera define el tiempo de animacion y si el jugador cambia de color
		public bool playerChange;

		// -- tmp VARS --
		public int score;  //vidas del jugador


		public Player ()
		{
			playerAction = "NR";
			playerAnimNumber = 0;
			playerWidth = 33;
			playerHeight = 58;
			playerMoveX = 0;
			playerMoveY = 0;
			playerType = 0;
			playerChange = false;
			score = 0;
		}

		// carga el contenid de jugador al inicio
		public void Load (BoxMan game)
		{

			this.playerSprite = game.Content.Load<Texture2D> ("player/" + "BoxMan" + playerAction + playerType + playerAnimNumber);
		}

		public void ReLoad (int startPositionX, int startPositionY)
		{
			this.playerRectangle = new Rectangle ( startPositionX , startPositionY, playerWidth , playerHeight);

		}

		// dibuja al jugador y su correspondiente animación
		public void Draw (SpriteBatch spriteBatch, Color gameColor)
		{
			spriteBatch.Draw (playerSprite,playerRectangle,gameColor);
		}
			

		// actualiza la posicion del jugador, controla estador
		public void Update(BoxMan game)
		{

			// Si el jugador no perdio y esta en movimiento
			if (this.playerAction != "ND" &&  this.playerMoveX != 0) 
			{
				if (game.gameCont % 40 == 0) {
					this.playerAnimNumber += 1;
					this.playerChange = true;
					if (this.playerAnimNumber > 1) {
						this.playerAnimNumber = 0;
					}
				}
			}

			// hubo una modificacion en "la animación" del jugador
			if (this.playerChange == true) {
				this.playerSprite = game.Content.Load<Texture2D> ("player/" + "BoxMan" + this.playerAction + this.playerType + this.playerAnimNumber);
				this.playerChange = false;
			}

			// actualizacion de pocición del jugador
			playerRectangle.Location = new Point (this.playerRectangle.Location.X + this.playerMoveX, this.playerRectangle.Location.Y + this.playerMoveY);

		}

	}
}

